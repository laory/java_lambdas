package com.education.special_simple;

import java.lang.invoke.LambdaMetafactory;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.util.function.Function;

public class ManualBootstrapping {

    private final int state;

    public ManualBootstrapping(int state) {
        this.state = state;
    }

    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Throwable {
        var lookup = MethodHandles.lookup();

        var methodHandle = lookup.findSpecial(ManualBootstrapping.class,
                "add",
                MethodType.methodType(Integer.class, Integer.class),
                ManualBootstrapping.class);

        var callSite = LambdaMetafactory.metafactory(lookup,
                "apply",
                MethodType.methodType(Function.class, ManualBootstrapping.class),
                MethodType.methodType(Object.class, Object.class),
                methodHandle,
                MethodType.methodType(Integer.class, Integer.class));

        /*
         * i -> i + field
         */
        Function<Integer, Integer> function1 = (Function<Integer, Integer>) callSite.dynamicInvoker().invokeExact(new ManualBootstrapping(1));
        Function<Integer, Integer> function2 = (Function<Integer, Integer>) callSite.dynamicInvoker().invokeExact(new ManualBootstrapping(2));

        System.out.println(function1.apply(5));
        System.out.println(function1.apply(8));
        System.out.println(function2.apply(5));
        System.out.println(function2.apply(8));
    }

    private Integer add(Integer num) {
        return num + state;
    }
}
