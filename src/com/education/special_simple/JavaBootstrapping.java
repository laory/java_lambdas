package com.education.special_simple;

import java.util.function.Function;

public class JavaBootstrapping {

    private final int state = 3;

    public void test() {
        Function<Integer, Integer> function = i -> i + state;

        Integer input = 5;

        Integer result = function.apply(input);

        System.out.println(result);
    }

    public static void main(String[] args) {
        new JavaBootstrapping().test();
    }
}
