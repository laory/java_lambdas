package com.education.static_complex;

import java.lang.invoke.LambdaMetafactory;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.util.function.Function;

public class ManualBootstrapping {

    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Throwable {
        var lookup = MethodHandles.lookup();

        var methodHandle = lookup.findStatic(ManualBootstrapping.class,
                "add",
                MethodType.methodType(Integer.class, Integer.class, Integer.class));

        var callSite = LambdaMetafactory.metafactory(lookup,
                "apply",
                MethodType.methodType(Function.class, Integer.class),
                MethodType.methodType(Object.class, Object.class),
                methodHandle,
                MethodType.methodType(Integer.class, Integer.class));
        Integer context = 9;

        /*
         * i -> i + context
         */
        Function<Integer, Integer> function = (Function<Integer, Integer>) callSite.dynamicInvoker().invokeExact(context);

        System.out.println(function.apply(5));
        System.out.println(function.apply(10));
    }

    private static Integer add(Integer i, Integer context) {
        return i + context;
    }
}
