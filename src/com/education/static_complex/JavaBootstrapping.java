package com.education.static_complex;

import java.util.function.Function;

public class JavaBootstrapping {

    public static void main(String[] args) {
        Integer context = 9;

        Function<Integer, Integer> function = i -> i + context;

        Integer input = 5;

        Integer result = function.apply(input);

        System.out.println(result);
    }
}
