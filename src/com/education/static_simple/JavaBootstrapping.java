package com.education.static_simple;

import java.util.function.Function;

public class JavaBootstrapping {

    public static void main(String[] args) {
        Function<Integer, Integer> function = i -> i + i;

        Integer input = 5;

        Integer result = function.apply(input);

        System.out.println(result);
    }
}
