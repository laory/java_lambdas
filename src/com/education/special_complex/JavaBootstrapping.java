package com.education.special_complex;

import java.util.function.Function;

public class JavaBootstrapping {

    private final int state = 3;

    public void test() {
        Integer context = 9;

        Function<Integer, Integer> function = i -> i + state + context;

        Integer input = 5;

        Integer result = function.apply(input);

        System.out.println(result);
    }

    public static void main(String[] args) {
        new JavaBootstrapping().test();
    }
}
