package com.education.special_complex;

import java.lang.invoke.CallSite;
import java.lang.invoke.LambdaMetafactory;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.util.function.Function;

public class ManualBootstrapping {

    private final int state;

    public ManualBootstrapping(int state) {
        this.state = state;
    }

    @SuppressWarnings("unchecked")
    public static void test(CallSite callSite, ManualBootstrapping statefulObj, Integer context) throws Throwable {
        /*
         * i -> i + state + context
         */
        Function<Integer, Integer> function = (Function<Integer, Integer>) callSite.dynamicInvoker().invokeExact(statefulObj, context);

        System.out.println(function.apply(5));
    }

    private Integer add(Integer i, Integer context) {
        return i + state + context;
    }

    public static void main(String[] args) throws Throwable {
        var lookup = MethodHandles.lookup();

        var methodHandle = lookup.findSpecial(ManualBootstrapping.class,
                "add",
                MethodType.methodType(Integer.class, Integer.class, Integer.class),
                ManualBootstrapping.class);

        var callSite = LambdaMetafactory.metafactory(lookup,
                "apply",
                MethodType.methodType(Function.class, ManualBootstrapping.class, Integer.class),
                MethodType.methodType(Object.class, Object.class),
                methodHandle,
                MethodType.methodType(Integer.class, Integer.class));

        ManualBootstrapping.test(callSite, new ManualBootstrapping(1), 3);
        ManualBootstrapping.test(callSite, new ManualBootstrapping(2), 4);
    }
}
